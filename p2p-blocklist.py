#!/usr/bin/env python
# Pyhon 3.7
# Twoure 10/22/2018
#
# Original: https://github.com/walshie4/Ultimate-Blocklist

import requests
import logging
from bs4 import BeautifulSoup as mksoup
import gzip
import os


BASE = "https://www.iblocklist.com"

def get_value_from(url):
    soup = mksoup(requests.get(BASE + url).text, features="html.parser")
    block_url_tag = soup.find("input", {"type":"text", "id": url.split("?list=")[-1]})
    return block_url_tag['value'] if block_url_tag else "unavailable"

def process(url):
    try:
        r = requests.get(url, allow_redirects=True, stream=True)
        r.raise_for_status()
    except Exception as e:
        logging.exception("URL open failed! Exception following:")
        return
    with open('ultBlockList.tmp.gz', 'wb') as out:
        for chunk in r.iter_content(chunk_size=1024):
            if not chunk:
                continue
            out.write(chunk)
    with gzip.open('ultBlockList.tmp.gz') as contents:
        with open("blocklist.txt", "a+b") as f:
            for line in contents:
                f.write(line)
    os.remove('ultBlockList.tmp.gz')

if __name__=="__main__":
    logging.basicConfig(
            format='%(asctime)s [%(levelname)s]: %(message)s',
            datefmt='%m/%d/%Y %I:%M:%S %p',
            filename='blocklist.log',
            level=logging.DEBUG
            )

    logging.info("Getting list page")

    url_lists = BASE + "/lists.php"
    try:
        r = requests.get(url_lists)
        r.raise_for_status()
    except Exception:
        logging.exception("Cannot open " + url_lists)
        raise

    soup = mksoup(r.text, features="html.parser")
    links = {}  #dict of name of list -> its url
    for row in soup.find_all("tr")[1:]:  #for each table row
        section = str(list(row.children)[0])
        pieces = section.split("\"")
        links[pieces[4].split("<")[0][1:]] = pieces[3]

    for link in links:  #download and combine files
        logging.info("***  Downloading " + link + " blocklist. ***")
        value = get_value_from(links[link])
        if value == "subscription":
            logging.warning("Blocklist is not available for free download D:")
        elif value == "unavailable":
            logging.warning("URL is unavailable")
        else:  #download and add this sucker
            process(value)
    logging.info("Finished building blocklist.txt")
