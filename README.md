# [P2P-Blocklist](https://twoure.gitlab.io/p2p-blocklist/)

Aggregate of popular P2P blocklists.

- [blocklist.txt](https://twoure.gitlab.io/p2p-blocklist/blocklist.txt)
- [blocklist.log](https://twoure.gitlab.io/p2p-blocklist/blocklist.log)
